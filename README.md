# inactiveP5AccountFinder.py

## Introduction

This is a script that takes a file formated in the style of `/etc/group` (see `groups_example.txt` for an example) and for each group queries the CERN Phonebook whether the members are still likely to be active in CMS.
The status is currently determined in three ways:

* Exists in CERN Phonebook
* Status is active in CERN Phonebook
* Associated to the EP department

The script then produces a file (`query_results.txt` by default) containing the user names of the members as well as a separate list for each "category of inactivity".

## Usage

Usage of the script should be quite intuitive:

```
python3 inactiveP5AccountFinder.py --help
Usage: inactiveP5AccountFinder.py [OPTIONS] LISTOFGROUPS

Options:
  --outputfile FILENAME
  --loglevel [DEBUG|INFO|WARN|ERR]
  --help                          Show this message and exit.
```

The argument `LISTOFGROUPS` should be the path to the input file formated in the style of `/etc/group`.

