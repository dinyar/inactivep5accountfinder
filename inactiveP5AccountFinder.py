#!/bin/env python3

from enum import IntEnum
import click
import subprocess

class LogLevel(IntEnum):
    DEBUG = 1
    INFO = 2
    WARN = 3
    ERR = 4
_log_level = None

_phonebook_search_cache = {}

@click.command()
@click.argument('listofgroups', type=click.File('r'))
@click.option('--outputfile', type=click.File('w'), default="query_results.txt")
@click.option('--loglevel', type=click.Choice(['DEBUG', 'INFO', 'WARN', 'ERR']), default='INFO')
def main(listofgroups, outputfile, loglevel):
    global _log_level
    _log_level = LogLevel[loglevel]
    num_groups = 0
    num_users = 0
    for l in listofgroups:
        debug(l)
        if len(l.strip()) == 0:
            continue;
        num_groups += 1
        tokenized = l.strip().split(':')
        group = tokenized[0]
        users = tokenized[3].strip().split(',')
        info(f"Working on group {group} with users: {users}.")
        if len(users) == 0 or users == ['']:
            continue
        removedUsers = []
        inactiveUsers = []
        nonEPUsers = []
        for user in users:
            num_users += 1
            info(f"Working on {user}")
            if not existsInPhonebook(user):
                info("Doesn't exist in Phonebook")
                removedUsers.append(user)
            elif not activeInPhonebook(user):
                info("Not active in Phonebook")
                inactiveUsers.append(user)
            elif not listedInEPInPhonebook(user):
                info("Not in EP department")
                nonEPUsers.append(user)
        constructMessage(group, users, removedUsers, inactiveUsers, nonEPUsers, outputfile)
    info(f"Done processing. Ran over {num_groups} groups with a total of {num_users} users.")

def existsInPhonebook(user):
    status = getUserStatusFromPhonebook(user)
    debug(f"Checking existence of {user}. Status from Phonebook is {status}.")
    if len(status) > 0:
        return True
    else:
        return False

def activeInPhonebook(user):
    status = getUserStatusFromPhonebook(user)
    debug(f"Checking if {user} is active. Status from Phonebook is {status}.")
    if '512' in status:
        return True
    else:
        return False

def listedInEPInPhonebook(user):
    departments = getUserDepartmentsFromPhonebook(user) 
    debug(f"Checking if {user} is in EP. Department from Phonebook is {departments}.")
    if 'EP' in departments:
        return True
    else:
        return False

def getUserStatusFromPhonebook(user):
    return getUserDataFromPhonebook(user, 'status')

def getUserDepartmentsFromPhonebook(user):
    return getUserDataFromPhonebook(user, 'departments')

def getUserDataFromPhonebook(user, dataKey):
    if user not in _phonebook_search_cache or dataKey not in _phonebook_search_cache[user]:
        debug("Cache miss, getting data from Phonebook.")
        user_data = {}
        data = subprocess.run(["/bin/phonebook", "--terse", "uac", "--terse", "department", "--login", user],
            		    stdout=subprocess.PIPE,
                                universal_newlines=True).stdout.split(';')
        if len(data) < 2:
            user_data['status'] = []
            user_data['departments'] = []
        else:
            user_data['status'] = data[0].split(',')
            user_data['departments'] = data[1].split(',')
        debug(f"Status for user {user}: {user_data['status']}")
        debug(f"Departments for user {user}: {user_data['departments']}")
        _phonebook_search_cache[user] = user_data
    return _phonebook_search_cache[user][dataKey]

def constructMessage(group, allUsers, removedUsers, inactiveUsers, nonEPUsers, outfile):
    outfile.write(f"{group} members: {allUsers}\n")
    if len(removedUsers) > 0:
        outfile.write(f"    Not in Phonebook: {removedUsers}\n")
    if len(inactiveUsers) > 0:
        outfile.write(f"    Disabled in Phonebook: {inactiveUsers}\n")
    if len(nonEPUsers) > 0:
        outfile.write(f"    Not in EP department: {nonEPUsers}\n")

def debug(msg):
    if _log_level <= LogLevel.DEBUG:
        print(f"# DEBUG: {msg}")

def info(msg):
    if _log_level <= LogLevel.INFO:
        print(f"# INFO: {msg}")

def warn(msg):
    if _log_level <= LogLevel.WARN:
        print(f"# WARNING: {msg}")

def err(msg):
    if _log_level <= LogLevel.ERR:
        print(f"# ERROR: {msg}")

if __name__ == "__main__":
    main()
